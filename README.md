# 동화면세점 모바일 국문

## node.js 설치
* node.js : [https://nodejs.org/en/download/](https://nodejs.org/en/download/)

## Gulp 설치
```
npm i -g gulp
```
```
npm i -D gulp gulp-connect
```

## 폴더 구조
```
node_modules/
src/
	index.html
	_templates.js
	_include.js
	_templates/
	html/
	images/
	scripts/
		libs/
	css/
		libs/
```

## handlebars

### 설치
```
npm install --save handlebars
```

### 컴파일
[http://handlebarsjs.com/precompilation.html](http://handlebarsjs.com/precompilation.html)
```
handlebars <input> -f <output>
```

```
handlebars src/_templates -f src/_templates.js
```

### html 파일 설정
```
<html>
<head>
</head>
<body>
	<div class="include" data-file="../../_templates/_test.handlebars"></div>
	<!-- 
	컨텐츠 
	-->

<!-- 개발적용시 제거 -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.6/handlebars.js"></script>
<script type="text/javascript" src="../../_templates.js"></script>
<script type="text/javascript" src="../../_include.js"></script>
</body>
</html>
```