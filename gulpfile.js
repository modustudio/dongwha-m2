/*
npm i -D gulp gulp-connect
*/

var gulp = require('gulp')
	connect = require('gulp-connect');
	autoprefixer = require('gulp-autoprefixer');

// Path
var path = './src';

// Server
gulp.task('serve', function() {
	connect.server({
		root: path,
		port: 8001,
		livereload: true,
		open: {
			browser: 'chrome'
		}
	});
});

gulp.task('autoprefixer', function() {
	gulp.src([path+'/css/**/*.css', '!'+path+'/css/libs/*.css'])
		.pipe(autoprefixer({
			browsers: ['last 3 versions','IE 8','android 2.3'],
			cascade: false
		}))
		.pipe( gulp.dest(path+'/css-autoprefixer'))
});

gulp.task('taskkill', function() {
  connect.serverClose();
  process.exit();
});

gulp.task('default', ['serve']);
