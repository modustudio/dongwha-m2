jQuery.browser = {};
(function () {
	jQuery.browser.msie = false;
	jQuery.browser.version = 0;
	if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
		jQuery.browser.msie = true;
		jQuery.browser.version = RegExp.$1;
	}
})();

var date = {
	yyyymmdd : function(){
		
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();

		if(dd<10) {
			dd = '0'+dd
		} 

		if(mm<10) {
			mm = '0'+mm
		} 

		return yyyy+mm+dd;
	},

	yyyymmddBar : function(){
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();
	
		if(dd<10) {
			dd = '0'+dd
		} 
	
		if(mm<10) {
			mm = '0'+mm
		} 
	
		return yyyy+'-'+mm+'-'+dd;
	},
	
	/*
	 * 날짜계산 년수 차이 계산
	 * a : 계산할 날짜 YYYY-MM-DD
	 */
	
	
	yearDiff : function(a){
		var inputDate = new Date(a.split('-')[0], a.split('-')[1], a.split('-')[2]);
		var today = new Date();
		
		var diff = today - inputDate;
		
		var currDay = 24 * 60 * 60 * 1000;// 시 * 분 * 초 * 밀리세컨
		var currMonth = currDay * 30;// 월 만듬
		var currYear = currMonth * 12; // 년 만듬
		
		return parseInt(diff/currYear);
	}
}

/*
//dotdotdot
var dot = function(){
	$('.productDot').each(function(){
		_$this = $(this);
		var h = (_$this.css("max-height") != "none") ? parseInt(_$this.css("max-height")) : null;
		_$this.dotdotdot({
			watch : true,
			height : h
		});
	});
};

$(function() {
	dot();
});
*/
// footer fixed
// $(document).ready(function(){
// 	if ($(".fix-footer").length > 0) {
// 		$(".wrap").css({"padding-bottom":"90px"});
// 	}
// });

//spinner
$(function(){
	$.fn.spinner = function() {
		this.each(function() {
			var el = $(this);
			if (el.closest(".spinner").length > 0) return;

			// add elements
			el.wrap('<span class="spinner"></span>');
			el.before('<span class="sub">&#45;</span>');
			el.after('<span class="add">&#43;</span>');

			el.parent().on('click', '.sub, .add', function () {

				var className = $(this).attr('class');
				if(className == "sub"){
					if (el.val() > parseInt(el.attr('min')))
						el.val( function(i, oldval) { return --oldval; });
				}else{
					if (el.attr('max')) {
						if (el.val() < parseInt(el.attr('max'))) 
							el.val( function(i, oldval) { return ++oldval; });
					} else { 
						el.val( function(i, oldval) { return ++oldval; });
					}
				}
				// 스피너 버튼 클릭시 수정버튼 생김
				if (el.closest(".spinner-object").siblings(".spinner-edit")) el.closest(".spinner-object").siblings(".spinner-edit").addClass("is-show");
			});

		});
	};
	$('.spinner-style').spinner();
});


//tab
var tab = function() {
	//if ($("[data-ui-tab='true']").length > 0) {
		$("[data-ui-tab='true']").each(function() {
			var $tab = $(this);
			var hash = window.location.hash;
			var tabArray= [];
			$tab.find("a[href^=#]").each(function(idx){
				tabArray.push($tab.find("a[href^=#]").eq(idx).attr("href"));
			});
			for (var i in tabArray) {
				$(tabArray[i]).removeClass("is-show").addClass("is-hide");
			}
			if (tabArray.indexOf(hash) != -1) {
				sele($tab.find("a[href="+hash+"]"),"is-on","is-on");
				$(hash).removeClass("is-hide").addClass("is-show");
			} else {
				if ($tab.children().hasClass("is-on")) {
					$($tab.children(".is-on").find("a[href^=#]").attr("href")).removeClass("is-hide").addClass("is-show");
				} else {
					sele($tab.find("a[href^=#]").eq(0),"is-on","is-on");
					$(tabArray[0]).removeClass("is-hide").addClass("is-show");
				}
			}
		});
	//}
	$(document).on("click","[data-ui-tab='true'] a[href^=#]",function(e){
		e.preventDefault();
		
		var $tab = $(this).closest("[data-ui-tab='true']");
		var hash = window.location.hash;
		var tabArray= [];
		$tab.find("a[href^=#]").each(function(idx){
			tabArray.push($tab.find("a[href^=#]").eq(idx).attr("href"));
		});
		
		if ($tab.data("ui-hash")) {
			if (!$(this).parent().hasClass("is-on")) window.location.hash = ($(this).attr("href"));
		}
		sele($(this),"is-on","is-on");
		for (var i in tabArray) {
			$(tabArray[i]).removeClass("is-show").addClass("is-hide");
		}
		$($(this).attr("href")).removeClass("is-hide").addClass("is-show");
		
	});
	function sele(el,show,hide) {
		$(el).parent().addClass(show).siblings().removeClass(hide);
	}
};
$(function() {
	tab();
});



//공통 form 이펙트 효과
$(function(){
    $(".formwrap").find("input:not([type=radio],[type=checkbox]), select, textarea").on("focus blur", function(evt){
        if (evt.type === "focus") {
            $(this).closest(".formwrap").addClass("formwrap-focus");
        } else {
            $(this).closest(".formwrap").removeClass("formwrap-focus");
        }
    })
});


// checkbox checked
$(function(){
	$(".checkbox-label").on("click",function(){
		var thisCheckd = $(this).find(".checkbox-obj").is(":checked");
		if(thisCheckd){
			$(this).addClass("checkbox-label__is-on");
		}else{
			$(this).removeClass("checkbox-label__is-on");
		}
	});
});


// $(function(){
// 	var termsTxtHeight = 0;
// 	$(".join-termstxt__more a").click(function(){
// 		if($(this).hasClass("is-open")){
// 			$(this).parents(".join-termstxt").stop().animate({"height":termsTxtHeight+"px"},200);
// 			$(this).removeClass("is-open");
// 			$(this).html("전문보기");
// 		}else{
// 			termsTxtHeight = $(this).parents(".join-termstxt").height();
// 			var termstxtWrapHeight = $(this).parents(".join-termstxt").find(".join-termstxt--wrap").height();
// 			$(this).parents(".join-termstxt").stop().animate({"height":termstxtWrapHeight+"px"},200);
// 			$(this).addClass("is-open");
// 			$(this).html("접기");
// 		}
// 	});
// });

//menu1
$(function(){
	var $menu1Wrap = $(".js-menu1-wrap");
	var $menu1Item = $(".js-menu1-item");
	var menu1On = $menu1Item.filter(".is-on").index();
	var menu1Width = [];
	$menu1Item.each(function(){
		menu1Width.push($(this).offset().left)
	});
	if (menu1Width[menu1On] > menu1Width[1]) {
		$menu1Wrap.scrollLeft(menu1Width[menu1On - 1]);
	}
});

// menu2
$(function(){
	var $menu2Toogle = $(".js-menu2-toggle")
	var $menu2Wrap = $(".js-menu2-wrap")
	$menu2Toogle.on("click", function(evt){
		evt.preventDefault();
		if ($menu2Wrap.hasClass("is-on")) {
			$menu2Wrap.removeClass("is-on");
			// $("body").css({overflow:"auto"});
			$("body").removeClass("is-not-scroll");
		} else {
			$menu2Wrap.css({top: ($(this).offset().top)+($(this).outerHeight())+"px"}).addClass("is-on");
			//$("body").css({overflow:"hidden"});
			$("body").addClass("is-not-scroll");
		}
	})
});
//공통 form 이펙트 효과

	var formWrap = function(){
		var $wrap = $(".form-wrap");
		var $label = $wrap.find(".js-form-label");
		var $form = $wrap.find("input, select, textarea, button");
		var isHas = "is-has"
		var isOut = "is-out"
		var isOver = "is-over"
		
		$form.each(function(){
			if (((this.tagName === "INPUT" || this.tagName === "TEXTAREA" || this.tagName === "BUTTON") && !(this.type === "radio" || this.type === "checkbox") && $(this).val()) || ((this.type === "radio" || this.type === "checkbox") && (this.checked)) || ((this.tagName === "SELECT") && (this.selected == 1)) ) {
				$(this).closest($wrap).addClass(isHas);
				$(this).filter("[readonly]").closest($wrap).addClass("is-readonly");
			} 
		})
		$label.attr("tabindex","-1").on("click", function(evt){
			var _form = $(this).closest($wrap).find("input, select, textarea, button");
			formOut();
			$(this).closest($wrap).addClass(isHas).addClass(isOver).removeClass(isOut);
		}).on("focus",function(){
			if ($(this).closest($wrap).find("input, select, textarea").length > 0 ) {
				$(this).closest($wrap).find("input, select, textarea")[0].focus();
				if ($(this).closest($wrap).find(".datepicker-btn").length > 0 ) { 
					$(this).closest($wrap).find(".datepicker-btn")[0].click();
				}
			} 
		}); 
		$form.on("focus",function(evt){
			$(this).closest($wrap).find($label).trigger("click");
		})
		$(document).on("click",function(evt){
			if ($(evt.target).closest($wrap).hasClass(isOver) == false) formOut()
		});
		
		function formOut() {
			//var $item = $form.filter(function() {
			//	if (((this.tagName === "INPUT" || this.tagName === "TEXTAREA") && !(this.type === "radio" || this.type === "checkbox") && $(this).val()) || ((this.type === "radio" || this.type === "checkbox") && (this.checked)) || ((this.tagName === "SELECT") && (this.selectedIndex != -1))) return true;
			//})
			//$wrap.removeClass(isHas).removeClass(isOver).addClass(isOut);
			//$item.closest($wrap).removeClass(isOut).removeClass(isOver).addClass(isHas);
			$wrap.removeClass(isOver).removeClass(isOut);
		}
		$(':radio[readonly]:not(:checked)').attr('disabled', true);
	} 
$(function() {
	formWrap()
})


//Dialog
$(function(){
	var dialogState = 0;
	var dialog = "dialog";
	var datepickerWrap = 'datepicker-wrap';
	var datepickerBtn = 'datepicker-btn';
	var datepickerState = 0;
	
	$(document).on("click","[data-ui-dialog]",function(e){
		var currentTop = $('html').scrollTop();
		tab();
		//dot();
		e.preventDefault();
		e.stopPropagation();
		
		var $t = this.getAttribute('href') ? this.getAttribute('href') : $(this).data("ui-href");
		var m = $(this).data("ui-dialog");
		if (typeof m == "number") {
			$($t).find("."+dialog+"-layout").css("width",m+"px");
		} else if (m == "100%") {
			$($t).find("."+dialog+"-layout").css({"width":"calc(100vw - 10px)", "max-width":"360px"});
		} else if (m == "alert") {
			$($t).find("."+dialog+"-layout").css("width","300px");
		}
		
		if (m == "close") {
			$("body").removeClass("is-not-scroll").scrollTop($("body").attr('data-scroll'));
			// $(this).parents("."+dialog).css("visibility","hidden");
			$(this).parents("."+dialog).hide();
			dialogState--;
			if (dialogState == 0) {
				$(".dimmed").hide().remove();
				$('html').scrollTop($("body").attr('data-scroll'));
				$("body").removeAttr('data-scroll').removeAttr('style');
				$(".page-title").removeAttr('style');
			}
			datepickerState = 0;
			$("."+datepickerWrap).find('input').datepicker('destroy')
		} else if ((typeof m === "number") || (m == "alert") || (m == "100%")){
			if ((dialogState == 0) && ($(".dimmed").length <1)) $('<div class="dimmed"></div>').appendTo($(document.body)).end().show();
			$("body").addClass("is-not-scroll").css('top',-currentTop);
			if($("body").attr('data-scroll')==undefined) $("body").attr('data-scroll',currentTop);
			if ($(".page-title").hasClass("js-fixed")) $(".page-title").css({"position":"fixed","top":"0"})
			// $($t).css("visibility","visible");
			$($t).show();
			dialogState++;
		} else {
			alert("data-ui-dialog 에 정수값을 입력해주세요.");
		}
		
		if ($(this).hasClass(datepickerBtn)) {
			var obj = 
			{
				inline: true,
				container: $('.datepicker-view'),
				autoHide: true,
				//autoshow: true,
				format: 'yyyy-mm-dd',
				daysMin: ['일', '월', '화', '수', '목', '금', '토'],
				yearFirst: true,
				yearSuffix: '년 ',
				monthsShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
			}
			if($(this).attr("startDate") == 'today'){
				obj["startDate"] = date.yyyymmddBar();
			}
			if (datepickerState == 0) {
				
				$(this).closest("."+datepickerWrap).find('input').datepicker(obj).datepicker('show')
			} else { 
				$(this).closest("."+datepickerWrap).find('input').datepicker('destroy')
			}

		}
		
	});
	
	$("."+datepickerWrap).find('input').on('click', function(){
		datepickerState = 0;
		$(this).closest("."+datepickerWrap).find("."+datepickerBtn).trigger('click');
		//console.log(111);
	}).on('show.datepicker', function() {
		datepickerState = 1;
		//$(this).closest("."+datepickerWrap).find("."+datepickerBtn).trigger('click');
		// console.log(cnt)
		//if ((dialogState == 0) && ($(".dimmed").length <1)) $('<div class="dimmed"></div>').appendTo($(document.body)).end().show();
		//$($(this).closest(datepickerWrap).find(datepickerBtn).data('ui-href')).css("visibility","visible");
		//dialogState++;
	}).on('hide.datepicker', function() {
		// $($(this).closest("."+datepickerWrap).find("."+datepickerBtn).data('ui-href')).css("visibility","hidden");
		$($(this).closest("."+datepickerWrap).find("."+datepickerBtn).data('ui-href')).hide();
		$("body").removeClass("is-not-scroll").scrollTop($("body").attr('data-scroll'));
		dialogState--;
		datepickerState = 0;
		if (dialogState == 0) {
			$(".dimmed").hide().remove();
		}
		// $t.datepicker('show');
	})

	
	/*
	if ($("."+datepickerBtn)) {
		// $("."+datepickerBtn).closest("."+datepickerWrap).find('input').datepicker({
		// 	inline: true,
		// 	container: $('.test'),
		// 	autoHide: true,
		// 	//autoshow: true,
		// 	format: 'yyyy-mm-dd',
		// 	daysMin: ['일', '월', '화', '수', '목', '금', '토'],
		// 	yearFirst: true,
		// 	yearSuffix: '년 ',
		// 	monthsShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월']
		// })
		.on('show.datepicker', function() {
			cnt = 1;
			//$(this).closest("."+datepickerWrap).find("."+datepickerBtn).trigger('click');
			// console.log(cnt)
			// if ((dialogState == 0) && ($(".dimmed").length <1)) $('<div class="dimmed"></div>').appendTo($(document.body)).end().show();
			// $($(this).closest(datepickerWrap).find(datepickerBtn).data('ui-href')).css("visibility","visible");
			// dialogState++;
		}).on('hide.datepicker', function() {
			// cnt = 0;
			// console.log(cnt)
			// $($(this).closest("."+datepickerWrap).find("."+datepickerBtn).data('ui-href')).css("visibility","hidden");
			// dialogState--;
			// if (dialogState == 0) {
			// 	$(".dimmed").hide().remove();
			// }
			// $t.datepicker('show');
		})
		// .on('click', function(){
		// 	$(this).closest("."+datepickerWrap).find("."+datepickerBtn).trigger('click');
		// })
	}
	*/
	// $(datepickerBtn).on('click', function(e) {
	// 	var $t = $(this).prev();
	// 	if (cnt == 1) {
	// 		e.stopPropagation();
	// 		$t.datepicker('hide');
	// 	} else if (cnt == 0) {
	// 		e.stopPropagation();
	// 		$t.focus();
	// 		$t.datepicker('show');
	// 	}
	//});
});


$(function(){
	$(".js-slidelink").on("click",function(e){
		var checkElement = $(this).parents(".js-sliderow").find(".js-slidecontent").is(':visible');
		if(checkElement){
			$(this).parents(".js-sliderow").find(".js-slidecontent").slideUp('fast');
		}else{
			$(this).parents(".js-sliderow").find(".js-slidecontent").slideDown('fast');
		}
		
		var getClass = $(this).attr("class");
		if ((0 < getClass.indexOf("arrow-up")) || (0 < getClass.indexOf("arrow-down"))) {
			var classArray = getClass.split(" ");
			for(var i=0; i<classArray.length; i++){
				if (0 < classArray[i].indexOf('arrow-up')){
					$(this).removeClass(classArray[i]);
					$(this).addClass(classArray[i].replace("arrow-up","arrow-down"));
				} else if (0 < classArray[i].indexOf("arrow-down")) {
					$(this).removeClass(classArray[i]);
					$(this).addClass(classArray[i].replace("arrow-down","arrow-up"));
				}
			}
		}
	});
});


function planSlidePager (currentSlide){
	var imgCnt = $(".img-slide__item").not(".bx-clone").length;
	var createHtml = '';
	$(".img-slide__item").not(".bx-clone").find("img").each(function(index) {
		if(index == currentSlide){
			createHtml += '<a data-slide-index="'+index+'" href="#n" class="active"></a>';
		}else if (index != (imgCnt -1 )){
			createHtml += '<a data-slide-index="'+index+'" href="#n"></a>';
		}else{
			createHtml += '<a data-slide-index="'+(index-1)+'" href="#n"></a>';
		}
	});
	$(".img-slide__pager").append(createHtml);
}
$(document).ready(function(){
	if ($(".img-slide__list").length > 0) {
		var planSlide = $('.img-slide__list').bxSlider({
			pagerCustom: '.img-slide__pager',
		autoControlsSelector:'.img-slide__play',
			controls: false,
			auto: true,
			autoControls: true,
		autoHover:true,
		hideControlOnEnd:true,
			startSlide: 2,
			pause : 1000
		});
		planSlidePager(planSlide.getCurrentSlide());
	}
});




//상단 카테고리 서브영역 열고 펼치기
$(document).on('click','.js-category, .category-list__close a',function(){
	var checkElement = $('.category-list');
	if(checkElement.is(':visible')) {
		checkElement.slideUp(200);
		$('.page-location__right-button2').removeClass("is-on");
		return false;
	}else if(!checkElement.is(':visible')) {
		checkElement.slideDown(200);
		$('.page-location__right-button2').addClass("is-on");
		return false;
	}
});

//리스트 뿌리는 방식 블럭or리스트
$(document).on('click','.info-area__list-type-link',function(){
	var findBlock = $(this).hasClass('info-area__list-type-link--block');
	//2017-08-11 김선중 수정
	//$('.item-list').hide();
	if(findBlock) {
		$(this).addClass('info-area__list-type-link--list').removeClass('info-area__list-type-link--block');
		//2017-08-11 김선중 수정
		//$('.item-list--list').show();
		return false;
	}else {
		$(this).addClass('info-area__list-type-link--block').removeClass('info-area__list-type-link--list');
		//2017-08-11 김선중 수정
		//$('.item-list--block').show();
		return false;
	}
});

//카테고리 sns버튼
$(document).on('click','.sns-circle__ico-sns',function(){
	var findOpen = $(this).hasClass('sns-circle__ico-sns--open');
	if(findOpen) {
		$(this).removeClass('sns-circle__ico-sns--open');
		$('.sns-circle__sns-group').fadeOut(150);
		return false;
	}else {
		$(this).addClass('sns-circle__ico-sns--open');
		$('.sns-circle__sns-group').fadeIn(150);
		return false;
	}
});

/* 헤더 사이드 메뉴 */
$(document).ready(function(){
	var headerMenuState = 0;
	$('.header__menu a').on('click',function(){
		if ((headerMenuState == 0) && ($(".dimmed-header-menu").length <1)) $('<div class="dimmed-header-menu"></div>').appendTo($(document.body)).end().show();
		$("body").addClass("is-not-scroll");
		$(".side-menu").addClass('is-on');
		headerMenuState++;
	});
	$(document).on('click','.dimmed-header-menu, .side-menu__close',function(){
		$(".side-menu").removeClass('is-on');
		setTimeout( function() {
			headerMenuState--;
			if (headerMenuState == 0) $(".dimmed-header-menu").hide().remove();
			$("body").removeClass("is-not-scroll");
		}, 500);
	});
	swipedetect($('.side-menu')[0], function(e){
			if (e === 'left') {
				$(".side-menu").removeClass('is-on');
				setTimeout( function() {
					headerMenuState--;
					if (headerMenuState == 0) $(".dimmed-header-menu").hide().remove();
					$("body").removeClass("is-not-scroll");
				}, 500);
			} 
	})

	/* 헤더 검색 메뉴 */
	// $(".header__search a").on('click', function(){
	// 	if ((headerMenuState == 0) && ($(".dimmed-header-menu").length <1)) $('<div class="dimmed-header-menu"></div>').appendTo($(document.body)).end().show();
	// 	$("body").css("overflow","hidden");
	// 	$(".search-popup").css({"visibility":"visible"});
	// 	headerMenuState++;
	// });
	// $(".option-area__close").on("click", function(){
	// 	headerMenuState--;
	// 	if (headerMenuState == 0) $(".dimmed-header-menu").hide().remove();
	// 	$("body").css("overflow","");
	// 	$(".search-popup").css({"visibility":"hidden"});
	// });
});

/* 꽉찬 레이어팝업 */
// $(document).ready(function(){
// 	var dimmedState = 0;				
// 	if($(".event-more").length > 0){
// 		$(".layer-popup-btn").on('click', function(){
// 			var thisId = $(this).data("id");
// 			if ((dimmedState == 0) && ($(".dimmed").length < 1)) $('<div class="dimmed"></div>').appendTo($(document.body)).end().show();
// 			$("body").css("overflow", "hidden");
// 			$("."+thisId).css({"visibility":"visible"});
// 			dimmedState++;
// 		});
// 		$(".layer-popup-close").on("click", function(){
// 			dimmedState--;
// 			var thisId = $(this).data("id");
// 			if (dimmedState == 0) $(".dimmed").hide().remove();
// 			$("body").css("overflow","");
// 			$(this).parent("");
// 			$("."+thisId).css({"visibility":"hidden"});
// 		});
// 	}
// });

/* 상세검색영역 */
$(document).ready(function(){
	$(".info-area__search-btn").on("click",function(){
		var closeFlag = $(this).hasClass("info-area__search-btn--close");
		if(closeFlag) {
			$(this).css({"z-index":0});
			$(this).html("상세검색");
			$(this).removeClass("info-area__search-btn--close");
			return false;
		}else{
			$(this).css({"z-index":21});
			$(this).empty();
			$(this).addClass("info-area__search-btn--close");
			return false;
		}
	});
});

//상단 카테고리 서브영역 열고 펼치기
$(document).ready(function(){
	var dimmedState = 0;				
	if($(".lenear__more").length > 0){
		$(".lenear__more, .lenear__more-close").on('click', function(){
			var checkElement = $('.lenear__full');
			if(checkElement.is(':visible')) {
				dimmedState--;
				if (dimmedState == 0) $(".dimmed").hide().remove();
				$(".header-wrap").removeAttr('style');
				$(".page-location").removeAttr('style');
				$(".lenear").removeAttr('style');
				checkElement.hide();
				$('.lenear__more').removeClass("is-on");
				return false;
			}else if(!checkElement.is(':visible')) {
				if ((dimmedState == 0) && ($(".dimmed").length < 1)) $('<div class="dimmed"></div>').appendTo($(document.body)).end().show();
				dimmedState++;
				$(".header-wrap").css({"z-index":510});
				$(".page-location").css({"z-index":509});
				$(".lenear").css({"z-index":508});
				
				checkElement.show();
				$('.lenear__more').addClass("is-on");
				return false;
			}
		});
	}
});

function isIE () {
	var myNav = navigator.userAgent.toLowerCase();
	return (myNav.indexOf('msie') != -1) ? parseInt(myNav.split('msie')[1]) : false;
}

$(function(){
	// if ($(".tab-list-content").length > 0 && $(".tab-list-content").hasClass("js-state") == false) {
	if ($(".tab-list-content").length > 0 ) {
		$(".tab-list-content").each(function(){
			var wrap = $(this);
			var item = wrap.find(".tab-list-content__item");
			if (isIE () && isIE() <= 8) {
				var regx = new RegExp(/tab-list-content_set-([0-9]*)/)
				var num = regx.exec(wrap.attr("class"))[1];
				for (var j = 0; j < item.length; j++) {
					wrap.find(item).eq(j).addClass("order-"+((j%num)+1));
				}
			}
			//wrap.addClass("js-state");
			// wrap.find("[tabindex=0]").on("mouseenter focusin",function(e){
			// 	$(this).closest(".tab-list-content__item").siblings().removeClass("is-on").end().addClass("is-on");
			// });
			// wrap.on("mouseleave",function(){
			// 	item.removeClass("is-on");
			// })
			wrap.find("[tabindex=0]").on("click keypress",function(e){
				if ((e.keyCode == 13)||(e.type == 'click')) {
					if ($(this).closest(item).hasClass("is-on")) {
						$(this).closest(item).removeClass("is-on");
					} else {
						$(this).closest(item).siblings().removeClass("is-on").end().addClass("is-on");
					}
				}
			});
			$(document).on("click",function(e){
				if (item.hasClass("is-on") && $(e.target).closest(item).hasClass("is-on") == false) {
					item.removeClass("is-on");
				}
			});
		})
	}
})

// 더보기
$(function(){
	$(".js-more-button").on("click","button",function(){
		if ($(this).closest(".js-more-button").hasClass("is-on")) {
			$(this).closest(".js-more-button").removeClass("is-on").siblings(".js-more-content").removeClass("is-view").scrollTop(0);
		} else { 
			$(this).closest(".js-more-button").addClass("is-on").siblings(".js-more-content").addClass("is-view");
		}
	});
});

// form-wrap3
$(function(){
	$(".form-wrap3__row").on("focus","input, select, textarea, button",function(){
		$(".form-wrap3__row").removeClass("is-focus");
		$(this).closest(".form-wrap3__row").addClass("is-focus");
	});
	$(document).on("click",function(evt){
		if ($(evt.target).closest(".form-wrap3__row").hasClass("is-focus") == false) {
			$(".form-wrap3__row").removeClass("is-focus")
		}
	});
});

// form-wrap4
$(function(){
	$(".table8").on("focus","input, select, textarea, button",function(){
		$(".table8 tr").removeClass("is-focus");
		$(this).closest("tr").addClass("is-focus");
	});
	$(document).on("click",function(evt){
		if ($(evt.target).closest(".table8 tr").hasClass("is-focus") == false) {
			$(".table8 tr").removeClass("is-focus")
		}
	});
});


$(function(){
	$(".js-checked").on("change","input",function() {
		if (this.checked) {
			$(this).closest(".js-checked").siblings(".js-checked-add").hide().end().next(".js-checked-add").show();
		} 
		
	})
})

//input:file
$(document).ready(function() {
	//preview image
	var imgTarget = $('.upload-hidden');

	imgTarget.on('change', function() {
		var parent = $(this).parent().parent();
		parent.children('.upload-display').remove().end().removeClass('upload-set--thumb');

		if (window.FileReader) {
			//image 파일만
			if (!$(this)[0].files[0].type.match(/image\//)) return;

			var reader = new FileReader();
			reader.onload = function(e) {
				var src = e.target.result;
				parent.prepend('<span class="upload-display"><img src="' + src + '" class="upload-thumb"><button type="button" class="upload-set__reset upload-reset"></button></span>').addClass('upload-set--thumb');
			}
			reader.readAsDataURL($(this)[0].files[0]);
		}
	
	});
	$(document).on('click','.upload-reset',function(){
		$(this).closest('.upload-set').removeClass('upload-set--thumb').children('.upload-display').remove();
		if ($.browser.msie) { 
			imgTarget.replaceWith( imgTarget.clone(true) ); 
		} else { 
			imgTarget.val(""); 
		}
	});

});

// dropDown
$(function(){
	var dropDown = ".drop-down1";
	var state = dropDown + "__state";
	var list =  dropDown + "__list";
	var item =  dropDown + "__item";
	var toggoleOpen = "is-open";

	$(state).attr("tabindex","0");
	$(state).on("click keypress",function(e){
		if ((e.keyCode == 13)||(e.type == 'click')) {
			if (!$(this).closest(dropDown).hasClass(toggoleOpen)) {
				$(this).closest(dropDown).addClass(toggoleOpen);
				$(this).closest(dropDown).append("<div class='"+dropDown.split(".",2)[1]+"__dimmed'></div>")
			} else { 
				$(this).closest(dropDown).removeClass(toggoleOpen);
				$(dropDown+"__dimmed").remove();
			}	
		}
	});
	$(item).on("click","a, button",function(e){
		$(this).closest(dropDown).find(state).text($(this).text());
		$(this).closest(dropDown).removeClass(toggoleOpen);
		$(dropDown+"__dimmed").remove();
	});
	$(document).on("click touchstart",function(e){
		if ($(e.target)[0] == $(dropDown+"__dimmed")[0] || ($(dropDown).hasClass(toggoleOpen) && $(e.target).closest(dropDown).hasClass(toggoleOpen) == false)) {
			$(dropDown).removeClass(toggoleOpen);
			$(dropDown+"__dimmed").remove();
		}
	});	

});

$(function(){
	$('.js-barcode-btn').on('click',function(e){
		e.preventDefault();
		if ($(this).hasClass('btn2-close-barcode')) {
			$(this).removeClass('btn2-close-barcode');
			$('.js-barcode-body').removeClass('is-on');
		} else {
			$(this).addClass('btn2-close-barcode');
			$('.js-barcode-body').addClass('is-on');
		}
	})
})

// 글자수 제한
$(function(){
	if ($(".js-check-length-input").length > 0 ) {
		$(".js-check-length-input").each(function(idx){
			$(this).on("keyup", function(){
				var limit = $(this).data("limit");
				var strLength = this.value.length;
				if(strLength > limit){
					this.value=this.value.substring(0,limit);
					this.focus();
					$(".js-check-length-output").eq(idx).html(limit);
				}else{
					$(".js-check-length-output").eq(idx).html(strLength); 
				}
			});
		});
	}
});

//footer
$(function(){
	$('.js-footer-more-btn').on('click',function(e) {
		e.preventDefault();
		if ($('.js-footer-more-list').hasClass('is-on')) {
			$('.js-footer-more-list').removeClass('is-on');
		} else { 
			$('.js-footer-more-list').addClass('is-on');
		}
	})
})


// 사이드바 카테고리
$(function() {
	$('.js-side-category1').find('.js-link').eq(0).parent().addClass('is-on').siblings().removeClass('is-on');
	$('.js-side-category2').eq(0).addClass('is-on').siblings().removeClass('is-on');
	$('.js-side-category1').find('.js-link').each(function(idx){
		var _parent = $(this).parent();
		$(this).on('click',function(e){
			e.preventDefault();
			_parent.siblings().removeClass('is-on').end().addClass('is-on');
			$('.js-side-category2').removeClass('is-on').eq(idx).addClass('is-on');
		})
	})
	$('.js-side-category2').on('click','.js-button',function(e){
		var _next = $(this).siblings('.js-list');
		if (_next.hasClass('is-on')) {
			$(this).removeClass('is-on');
			_next.removeClass('is-on');
		} else {
			$(this).addClass('is-on');
			_next.addClass('is-on');
		}
	});
});

// 사이드바 브랜드(한글)
$(function() {
	var brandKoState = 0;
	var brandKoIndex = 0;
	var brandKoScrollTop = 0;
	var brandKoHead, brandKoTop, brandKoScroll;
	var $item = $('.js-side-brand-ko-set').find('.js-link');
	var $head = $('.js-side-brand-ko-body').find('.js-head');
	var $set = $('.js-side-brand-ko-set');
	var $body = $('.js-side-brand-ko-body');
	
	if($body.size() != 0){
		if ($set.find('.is-on').length == 0) {
			$item.eq(brandKoIndex).parent().addClass('is-on').siblings().removeClass('is-on');
		}
		brandKoSetFnc();
	}
	
	$body.on('scroll',function(){
		brandKoHead = [];
		brandKoTop = $body.offset().top;
		brandKoScroll = $body.scrollTop();
		$head.each(function(){
			brandKoHead.push($(this).offset().top - brandKoTop + brandKoScroll);
		});
		if(brandKoScrollTop < $(this).scrollTop()){
			brandKoScrollTop = $(this).scrollTop();
			if(brandKoScrollTop >= brandKoHead[brandKoIndex + 1]){
				brandKoIndex++;
				brandKoSeleFnc();
			}
		}else{
			brandKoScrollTop = $(this).scrollTop();
			if(brandKoScrollTop < brandKoHead[brandKoIndex]){
				brandKoIndex--;
				brandKoSeleFnc();
			};
		};
	});
	
	$item.each(function(idx){
		$(this).on('click',function(e){
			e.preventDefault();
			if (brandKoState == 0) { 
				brandKoSetFnc();
				brandKoState = 1;
			}
			brandKoIndex = idx;
			$body.stop().animate({'scrollTop':brandKoHead[brandKoIndex]},function(){
				$(this).css({'overflow-y':'scroll'})
			});
			brandKoSeleFnc();
		});
	})
	
	function brandKoSetFnc(){ //초기 셋팅
		brandKoHead = [];
		brandKoTop = $body.offset().top;
		brandKoScroll = $body.scrollTop();
		$set.scrollTop(0);
		$body.scrollTop(0);
		$head.each(function(){
			brandKoHead.push($(this).offset().top - brandKoTop + brandKoScroll);
		});
	}
	
	function brandKoSeleFnc(){ //클릭할때마다
		$item.eq(brandKoIndex).parent().addClass('is-on').siblings().removeClass('is-on');
		$set.stop().animate({'scrollTop':(brandKoIndex)*41});
	}
	
});

// 사이드바 브랜드(한글)
$(function() {
	var brandEnState = 0;
	var brandEnIndex = 0;
	var brandEnScrollTop = 0;
	var brandEnHead, brandEnTop, brandEnScroll;
	var $item = $('.js-side-brand-en-set').find('.js-link');
	var $head = $('.js-side-brand-en-body').find('.js-head');
	var $set = $('.js-side-brand-en-set');
	var $body = $('.js-side-brand-en-body');
	
	if($body.size() != 0){
		if ($set.find('.is-on').length == 0) {
			$item.eq(brandEnIndex).parent().addClass('is-on').siblings().removeClass('is-on');
		}
		brandEnSetFnc();
	}
	
	$body.on('scroll',function(){
		brandEnHead = [];
		brandEnTop = $body.offset().top;
		brandEnScroll = $body.scrollTop();
		$head.each(function(){
			brandEnHead.push($(this).offset().top - brandEnTop + brandEnScroll);
		});
		if(brandEnScrollTop < $(this).scrollTop()){
			brandEnScrollTop = $(this).scrollTop();
			if(brandEnScrollTop >= brandEnHead[brandEnIndex + 1]){
				brandEnIndex++;
				brandEnSeleFnc();
			}
		}else{
			brandEnScrollTop = $(this).scrollTop();
			if(brandEnScrollTop < brandEnHead[brandEnIndex]){
				brandEnIndex--;
				brandEnSeleFnc();
			};
		};
	});
	
	$item.each(function(idx){
		$(this).on('click',function(e){
			e.preventDefault();
			if (brandEnState == 0) { 
				brandEnSetFnc();
				brandEnState = 1;
			}
			brandEnIndex = idx;
			$body.stop().animate({'scrollTop':brandEnHead[brandEnIndex]});
			brandEnSeleFnc();
		});
	})
	
	function brandEnSetFnc(){ //초기 셋팅
		brandEnHead = [];
		brandEnTop = $body.offset().top;
		brandEnScroll = $body.scrollTop();
		$set.scrollTop(0);
		$body.scrollTop(0);
		$head.each(function(){
			brandEnHead.push($(this).offset().top - brandEnTop + brandEnScroll);
		});
	}
	
	function brandEnSeleFnc(){ //클릭할때마다
		$item.eq(brandEnIndex).parent().addClass('is-on').siblings().removeClass('is-on');
		$set.stop().animate({'scrollTop':(brandEnIndex)*41});
	}
	
});


// 내부 스크롤 탑
$(function(){
	$('.js-go-to-top-body').on('scroll', function() {
		var _button = $(this).parent().find('.js-go-to-top-button');
		if ($(this).scrollTop() > 0) {
			_button.fadeIn(100);
		} else { 
			_button.fadeOut(100);
		}
	});
	
	$('.js-go-to-top-button').on('click','button, a, span', function(e){
		e.preventDefault();
		var _body = $(this).closest('.js-go-to-top-button').parent().find('.js-go-to-top-body');
		_body.stop().animate({'scrollTop':0},200);
	})
});

$(function(){
	$('<div id="console"></div>').appendTo('body').css({
		'position':'absolute',
		'top':0,
		'left':0,
		'right':0,
		'background-color':'black',
		'color':'white',
		'z-index':'9999'
	});
	//$('#console').html('console');
})


// 터치 이벤트
function swipedetect(el, callback){
	var touchsurface = el,
	swipedir,
	startX,
	startY,
	distX,
	distY,
	threshold = 150, //required min distance traveled to be considered swipe
	restraint = 100, // maximum distance allowed at the same time in perpendicular direction
	allowedTime = 300, // maximum time allowed to travel that distance
	elapsedTime,
	startTime,
	handleswipe = callback || function(swipedir){}
  
	touchsurface.addEventListener('touchstart', function(e){
		var touchobj = e.changedTouches[0]
		swipedir = 'none'
		dist = 0
		startX = touchobj.pageX
		startY = touchobj.pageY
		startTime = new Date().getTime() // record time when finger first makes contact with surface
		e.stopPropagation()
	}, false)
  
	touchsurface.addEventListener('touchmove', function(e){
		e.stopPropagation() // prevent scrolling when inside DIV
	}, false)
  
	touchsurface.addEventListener('touchend', function(e){
		var touchobj = e.changedTouches[0]
		distX = touchobj.pageX - startX // get horizontal dist traveled by finger while in contact with surface
		distY = touchobj.pageY - startY // get vertical dist traveled by finger while in contact with surface
		elapsedTime = new Date().getTime() - startTime // get time elapsed
		if (elapsedTime <= allowedTime){ // first condition for awipe met
			if (Math.abs(distX) >= threshold && Math.abs(distY) <= restraint){ // 2nd condition for horizontal swipe met
				swipedir = (distX < 0)? 'left' : 'right' // if dist traveled is negative, it indicates left swipe
			}
			else if (Math.abs(distY) >= threshold && Math.abs(distX) <= restraint){ // 2nd condition for vertical swipe met
				swipedir = (distY < 0)? 'up' : 'down' // if dist traveled is negative, it indicates up swipe
			}
		}
		handleswipe(swipedir)
		e.stopPropagation()
	}, false)
}

$(function(){
	$(document).on('click','.js-search',function(){
		if ($('.search-popup').hasClass('is-on')) {
			$(".dimmed").hide().remove();
			$('.search-popup').removeClass('is-on');
			$('body').removeClass('is-not-scroll');
		} else {
			$('body').addClass('is-not-scroll');
			$('.search-popup').addClass('is-on');
			if ($(".dimmed").length < 1) $('<div class="dimmed"></div>').appendTo($(document.body)).end().show();
		}
		
	})
})

// 메인 이벤트 전체보기
$(function(){
	$(document).on('click','.js-event-all',function(e){
		e.preventDefault();
		var $popup = $('.layer-popup-content');
		if ($popup.length > 0) {
			if ($popup.hasClass('is-on')) {
				$('body').removeClass('is-not-scroll');
				$popup.removeClass('is-on');
			} else {
				$('body').addClass('is-not-scroll');
				$popup.addClass('is-on');
			}
		}
	});
})

//page title 고정
$(window).scroll(function(){
	var sc_top = $(window).scrollTop()
	if(sc_top <= 65){
		$('.sub-cate__group, .page-title').removeClass("js-fixed");
	}else{
		$('.sub-cate__group, .page-title').addClass("js-fixed");
	}
});


function slidePager(item,target,currentSlide){
	item.not(".bx-clone").each(function(index) {
		var on = (index == currentSlide) ? ' active':''
		$('<a data-slide-index="'+index+'" href="#n" class="slide-bullet'+on+'"></a>').appendTo(target)
	});
}

$(document).ready(function(){
	var $detailWrap = $('.detail-slide')
	var $detaillist = $detailWrap.find($detailWrap.selector + '__list');
	var $detailItem = $detailWrap.find($detailWrap.selector + '__item');
	var _detailPager = $detailWrap.selector.split('.')[1]+'__pager';
	var _detailView = $detailWrap.selector.split('.')[1]+'__view-larger';
	$('<div class="'+_detailView+'"></div>').appendTo($detailWrap);
	
	$detailItem.find('img').each(function(idx){
		var on = (idx == 0) ? ' class="is-on"':''
		$('<a href="'+$(this).data('ui-href')+'"'+on+'>크게보기</a>').appendTo($('.'+_detailView))
	})
	
	if ($('.detail-slide__item').length > 1) {
		$('<div class="'+_detailPager+'"></div>').appendTo($detailWrap);
		var detailSlide = $detaillist.bxSlider({
			controls: false,
			pagerCustom: '.'+_detailPager,
			auto: false,
			pause : 2000,
			autoControls: true,
			hideControlOnEnd:true,
			autoHover:true,
			onSlideAfter: function(elem,oldIdx,newIdx){
				$('.'+_detailView).children('a').removeClass('is-on')
				$('.'+_detailView).children('a').eq(newIdx).addClass('is-on')
			},
		});
		slidePager($detailItem,'.'+_detailPager,detailSlide.getCurrentSlide());
	}
});

// 이벤트, 기획전 배너
$(function(){
	var $eventWrap = $('.event-banner')
	$($eventWrap).each(function(idx){
		var $eventList = $(this).find($eventWrap.selector + '__list');
		var $eventItem = $(this).find($eventWrap.selector + '__item');
		var _eventPager = $eventWrap.selector.split('.')[1]+'__pager';
		
		if ($eventItem.length > 1) {
			$('<div class="'+_eventPager+' '+_eventPager+'--'+idx+'"></div>').appendTo($(this));
			var eventSlide = $eventList.bxSlider({
				controls: false,
				pagerCustom: '.'+_eventPager+'--'+idx,
				auto: false,
				pause : 2000,
				autoControls: false,
				hideControlOnEnd:true,
				autoHover:true
			});
			slidePager($eventItem,'.'+_eventPager+'--'+idx,eventSlide.getCurrentSlide());
		}
	});
});

function quickToggle(target,cls) {
	$(target).toggleClass(cls);
}
