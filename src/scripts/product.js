function cautionSlidePager (currentSlide){
	var itemCnt = $(".caution-list>li").not(".bx-clone").length;
	var createHtml = '';
	$(".caution-list>li").not(".bx-clone").each(function(index) {
		if(index == currentSlide){
			createHtml += '<a data-slide-index="'+index+'" href="#n" class="active"></a>';
		}else{
			createHtml += '<a data-slide-index="'+index+'" href="#n"></a>';
		}
	});
	$(".caution-slide__pager").append(createHtml);
}
$(document).ready(function(){
	if ($(".caution-list").length > 0) {
		var cautionSlide = $('.caution-list').bxSlider({
			slideWidth: 1000,
			minSlides: 2,
			maxSlides: 2,
			moveSlides: 1,
			controls: false,
			pagerCustom: '.caution-slide__pager',
			auto: true,
			pause : 1000
		});
		cautionSlidePager(cautionSlide.getCurrentSlide());
	}
});



function brandstorySlidePager (currentSlide){
	var itemCnt = $(".brandstory-list>li").not(".bx-clone").length;
	var createHtml = '';
	$(".brandstory-list>li").not(".bx-clone").each(function(index) {
		if(index == currentSlide){
			createHtml += '<a data-slide-index="'+index+'" href="#n" class="active"></a>';
		}else{
			createHtml += '<a data-slide-index="'+index+'" href="#n"></a>';
		}
	});
	$(".brandstory-slide__pager").append(createHtml);
}
$(document).ready(function(){
	if ($(".brandstory-list").length > 0) {
		var brandstorySlide = $('.brandstory-list').bxSlider({
			slideWidth: 1000,
			minSlides: 2,
			maxSlides: 2,
			moveSlides: 1,
			controls: false,
			pagerCustom: '.brandstory-slide__pager',
			auto: true,
			pause : 1000
		});
		brandstorySlidePager(brandstorySlide.getCurrentSlide());
	}
});
